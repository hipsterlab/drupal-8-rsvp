<?php
/**
 * @file
 * Contains \Drupal\rsvplist\Form\RSVPForm
 */
namespace Drupal\rsvplist\Form;

use Drupal\Core\Database\Database;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides an RSVP Email form.
 */

class RSVPForm extends FormBase{

    // Databse connection
    protected $conn;

    public function __construct(\Drupal\Core\Database\Connection $conn)
    {
        $this->conn = $conn;
    }

//    public function setDatabaseConnection(\Drupal\Core\Database\Connection $conn){
//        $this->conn = $conn;
//    }

    public static function create(ContainerInterface $container)
    {
       return new static ($container->get('database'));
    }

    /**
     * (@inheritdoc)
     */
    public function getFormId(){
        return 'rsvplist_email_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $node = \Drupal::routeMatch()->getParameter('node');
        $nid = $node->nid->value;
        $form['email'] = array(
            '#title' => t('Email Address'),
            '#type' => 'textfield',
            'size' => '25',
            '#description' => t("We'll send updates to the mail address your provide."),
            '#required' => TRUE,
            '#placeholder' => 'your email address',
        );
        
        $form['submit'] = array(
            '#type'  => "submit",
            '#value' =>  "RSVP",
        );

        $form['nid'] = array(
            '#type' => 'hidden',
            '#value' => $nid,
        );

        return $form;
    }

    /**
     * (@inheritdoc)
     */

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $email = $form_state->getValue('email');
        if(!\Drupal::service('email.validator')->isValid($email)){
            $form_state->setErrorByName('email', t('The email address %mail is not valid. ', array('%mail' => $email)));
        }
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
//           drupal_set_message(t("The form is working."));
//           drupal_set_message(t("The form is working."), 'status');
//           drupal_set_message(t("The form is working."), 'warning');
//           drupal_set_message(t("The form is working."), 'error');

        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $query = $this->conn->insert('rsvplist');
        $query->fields(array(
            'mail' => $form_state->getValue('email'),
            'nid' => $form_state->getValue('nid'),
            'uid' => $user->id(),
            'created' => time())
        );

        try{

            $query->execute();
            drupal_set_message("Thanks for your RSVP, you are on the list for the event.");
        }catch (\Exception $e){
            drupal_set_message('Hubo un error en la aplicación', 'error');
        }

    }
}