<?php

/**
 * Created by PhpStorm.
 * User: ondeuev
 * Date: 27/02/2017
 * Time: 11:42
 */

namespace Drupal\rsvplist\plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Class RSVPBlock
 * @package Drupal\rsvplist\plugin\Block
 *
 * Provides an 'RSVP' List Block
 * @Block(
 *     id = "rsvp_block",
 *     admin_label = @Translation("RSVP Block")
 * )
 */
class RSVPBlock extends BlockBase implements ContainerFactoryPluginInterface
{

    /** @var   \Drupal\Core\Form\FormBuilderInterface */
    protected $formBuilder;

    /** @var  \Drupal\Core\Routing\RouteMatchInterface */
    protected $routeMatcher;

    public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder, RouteMatchInterface $routeMatcher){
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->formBuilder = $formBuilder;
        $this->routeMatcher = $routeMatcher;
    }

    public static function create(ContainerInterface $container,array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('form_builder'),
            $container->get('current_route_match')
        );
    }


    public function build()
    {
        return $this->formBuilder->getForm('Drupal\rsvplist\Form\RSVPForm');
//        return \Drupal::formBuilder()->getForm('Drupal\rsvplist\Form\RSVPForm');
    }

    public function blockAccess(AccountInterface $account)
    {
        /** @var \Drupal\node\Entity\Node $node */
        $node = $this->routeMatcher->getParameter('node');
        $nid = $node->nid->value;
        if(is_numeric($nid)){
            return AccessResult::allowedIfHasPermission($account,'view rsvplist');
        }
        return AccessResult::forbidden();

    }



}